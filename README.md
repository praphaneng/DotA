## General info
This test is for candidates who are interested to join SmartSoft Asia Co., Ltd. as a
software developer (frontend).

## Technologies
Project is created with:
* Node.js version: v16.14.2
* express version: ^4.18.1
* ejs version: ^3.1.8
* dotenv version: ^16.0.1
* axios version: ^0.27.2
* cookie-parser version: ^1.4.6
* express-session version: ^1.17.3
* jsonwebtoken version: ^8.5.1
* lodash version: "^4.17.21"

## Setup
To run this project, install it locally using npm:

```
$ npm install
$ npm start or node app.js
```
