const axios = require('axios').default;

exports.getHeroList = function() {
    return axios.get(process.env.API_DotA + '/heroStats')
        .then(function(res) {
            return res.data
        })
        .catch(function(error) {
            return error
        })
};

exports.getHeroDetailbyID = function(heroID) {
    return axios.get("https://www.dota2.com/datafeed/herodata?language=thai&hero_id=" + parseInt(heroID))
        .then(function(res) {
            return res.data
        })
        .catch(function(error) {
            return error
        })
};