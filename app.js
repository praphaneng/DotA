const express = require('express');
var app = express();
var session = require('express-session')
var _ = require('lodash');

require('dotenv').config()

app.set('trust proxy', 1) // trust first proxy
app.set('view engine', 'ejs');

app.use(express.static(__dirname + '/assets'));
app.use(express.json())
app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true,
    cookie: {
        secure: false
    }
}))

const heros_service = require('./controllers/heros_service/service.js');

app.get('/', async function(req, res) {
    try {
        let dataHeroList = heros_service.getHeroList();

        //I try to wait data cause it's sending pendding
        await dataHeroList.then(function(result) {
            dataHeroList = result;
        })

        dataHeroList.forEach(i => {
            i.img = "https://api.opendota.com" + i.img
        });

        res.render('pages/index', {
            data: dataHeroList,
        });
    } catch (err) {
        res.send(err)
    }
});

app.get('/hero/:heroid', async function(req, res) {
    try {
        let heroID = req.params.heroid;
        console.log("heroID : ", heroID)

        let getHeroDetailbyID = heros_service.getHeroDetailbyID(heroID);
        //I try to wait data cause it's sending pendding
        await getHeroDetailbyID.then(function(result) {
            getHeroDetailbyID = result;
        })

        console.log("getHeroDetailbyID : ", getHeroDetailbyID)


        res.render('pages/hero-detail', {
            heroname: getHeroDetailbyID.result.data.heroes[0].name_loc,
            data: getHeroDetailbyID,
        });
    } catch (err) {
        res.send(err)
    }
});

app.get('/fav-hero', async function(req, res) {
    try {
        let dataHeroList = heros_service.getHeroList();

        //I try to wait data cause it's sending pendding
        await dataHeroList.then(function(result) {
            dataHeroList = result;
        })

        dataHeroList.forEach(i => {
            i.img = "https://api.opendota.com" + i.img
        });

        if (req.session.favhero) {
            let favhero = req.session.favhero;

            /* Remove duplicate values */
            let uniq = _.uniq(favhero);

            let heroinfoArr = [];

            uniq.forEach(i => {
                let heroinfo = _.filter(dataHeroList, {
                    'id': parseInt(i)
                })
                heroinfoArr.push(heroinfo)
            })

            res.render('pages/fav-hero', {
                heroinfoArr: heroinfoArr
            });
        } else {
            res.render('pages/fav-hero');
        }
    } catch (err) {
        res.send(err)
    }
});

app.post('/add-infav', function(req, res) {
    try {
        let heroID = req.body.id;

        if (req.session.favhero) {
            req.session.favhero.push(heroID)

            console.log(" req.session.favhero : ", req.session.favhero)
            res.send({
                result: "ok"
            })
        } else {
            req.session.favhero = [];
            req.session.favhero.push(heroID);

            res.end('welcome to the session demo. refresh!')
        }
    } catch (err) {
        res.send(err)
    }
});

app.listen(8080);
console.log('8080 is Running...');